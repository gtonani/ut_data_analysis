Scripts:

- get_bad_channel_list.py: To analyse data from tests, save rms and identify bad channels

- make_table.py: plot tables




To run (~~~~ ATTENTION, to access the data (/eos/user/m/mtobin/UT/DATA/) you need to subscribe the ut mailinglist ~~~~): 

	lb-conda default bash

	python get_bad_channel_list.py (run on the layers separately, so you only have to change the input data path at line 153, the output filename line 157 and from 171 to 176 change "UTa" or "UTb" if you are running on IP of MAGNET respectively)

	python make_table.py (run on the layers separately, you only have to change the input filename at line 8)




Comments: code works for side C data 

Presentation of the preliminary results: https://docs.google.com/presentation/d/1qTPeNgz63vdUOM5fqNEMXNvaOX6dC1gW/edit?usp=sharing&ouid=110081792696878757010&rtpof=true&sd=true





