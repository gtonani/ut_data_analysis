from ROOT import *
import glob, math, os
import sys

"""
Test procedure:
1) Get mean of total & mcm noise distributions (1d histograms)
2) Get rms of total and mcm adc distributions
"""
def getBadChannels( h, nsigma=5 ):
    badChannels = []
    h.Fit("pol0")
    noise = h.GetFunction("pol0").GetParameter(0)
    error = h.GetFunction("pol0").GetParError(0)
    upper = noise + nsigma * error
    lower = noise - nsigma * error
    #fOut.write( "Mean = " + "{:6.2f}".format(noise) + " +/- " + "{:6.2f}".format(error) + " (fit result)\n" )
    #fOut.write( "Upper limit: " + "{:6.2f}".format(upper) + " ADC counts\n" )
    #fOut.write( "Lower limit: " + "{:6.2f}".format(lower) + " ADC counts\n" )
    for i in range(0, 128):
        value = h.GetBinContent( i + 1 )
        if value > upper:
            badChannels.append( ( i, 'high' ) )
        if value < lower:
            badChannels.append( ( i, 'low' ) )
    h.GetYaxis().SetRangeUser(0, 5)
    l.SetLineColor(2)
    l.SetLineStyle( 2 )
    l.SetLineWidth( 2 )
    l.DrawLine( 0, upper, 128, upper )
    l.DrawLine( 0, lower, 128, lower )
    c.Update()
    c.Print("a.ps")
    return badChannels

def getBadChannelsFromPed( h, noise, factor=2.5 ):
    badChannels = []
    h.Fit("pol0", "0")
    mean = h.GetFunction("pol0").GetParameter(0)
    upper = mean + factor * noise
    lower = mean - factor * noise
    #fOut.write( "Upper limit: " + "{:6.2f}".format(upper) + " ADC counts\n" )
    #fOut.write( "Lower limit: " + "{:6.2f}".format(lower) + " ADC counts\n" )
    for i in range( 0, 128 ):
        value = h.GetBinContent( i+1 )
        if value > upper:
            badChannels.append( (i, 'highAdc' ) )
        if value < lower:
            badChannels.append( (i, 'lowAdc' ) )
    l.SetLineColor(2)
    l.SetLineStyle( 2 )
    l.SetLineWidth( 2 )
    l.DrawLine( 0, upper, 128, upper )
    l.DrawLine( 0, lower, 128, lower )
    return badChannels



def badChannelAnalysis( fname ):

    print( "--------------------------------", fname )
    #fOut.write( "------------------------------------------------------------------------------------------------------\n " + fname)
    #fOut.write( "\n------------------------------------------------------------------------------------------------------\n")
    #fOut.write("\n")


    f = TFile(fname)
    try:
        totalNoise = f.Get("hTotNoiseDist").GetMean()
        cmsNoise   = f.Get("hCmsNoiseDist").GetMean()
        mcmNoise   = f.Get("hMCMDist_o").GetRMS()

        rawAdcRMS = f.Get("hAdcRawChan").GetRMS(2)
        cmsAdcRMS = f.Get("hAdcCmsChan").GetRMS(2)

        fOut.write(str(totalNoise))
        fOut.write("\t")
        fOut.write(str(cmsNoise))


        hProfRawAdc = f.Get("hAdcRawChan").ProfileX()
        hProfCmsAdc = f.Get("hAdcCmsChan").ProfileX()

    except:
        fOut.write(str(0))
        fOut.write("\t")
        fOut.write(str(0))
    try:
        hRawVsChan = f.Get("hTotNoiseChan")
        hRawVsChan.Fit("pol0")
        rawNoiseFit = hRawVsChan.GetFunction("pol0").GetParameter(0)
        rawNoiseErrFit = hRawVsChan.GetFunction("pol0").GetParError(0)
        badRaw = getBadChannels( hRawVsChan ,3)
        hCmsVsChan = f.Get("hCmsNoiseChan")
        hCmsVsChan.Fit("pol0")
        cmsNoiseFit = hCmsVsChan.GetFunction("pol0").GetParameter(0)
        cmsNoiseErrFit = hCmsVsChan.GetFunction("pol0").GetParError(0)
        badCms = getBadChannels( hCmsVsChan , 3)

        #f.Get("hAdcRawChan").Draw()
        badRawPed = getBadChannelsFromPed( hProfRawAdc, rawNoiseFit )

        badCmsPed = getBadChannelsFromPed( hProfCmsAdc, cmsNoiseFit )
 
        summary = {}
        for i in range(0, 128): summary[i] = []
        for i in badRaw:
            chan = i[0]
            summary[chan].append( i[1]+'RawNoise' )
        for i in badCms:
            chan = i[0]
            summary[chan].append( i[1]+'CmsNoise' )
        for i in badRawPed:
            chan = i[0]
            summary[chan].append( i[1]+'Raw' )
        for i in badCmsPed:
            chan = i[0]
            summary[chan].append( i[1]+'Cms' )

        conto=0
        for i in sorted(summary.keys()):
            if len(summary[i]) > 0:
                conto +=1
                print( i, summary[i] )
                if conto < 10:
                    #fOut.write( str(i) + "\t" + str(summary[i]) )
                    fOut.write("\t")
                    fOut.write( str(i))
        
        for i in range(9-conto):
            fOut.write( "\t"+"-")

        fOut.write("\t")
        fOut.write(str(conto))

    except:
        for i in range(9):
            fOut.write( "\t"+"-")
        fOut.write( "\t"+"0")
        pass

print( os.sys.argv )
names = os.sys.argv[1:]
c = TCanvas("c1","c1" )
#c.Print("a.ps[")
l = TLine()
t = TText()
gROOT.SetStyle("Plain");
gStyle.SetOptStat(0);
asicId = 999
sector = ""

names = glob.glob("/eos/user/m/mtobin/UT/DATA/IP/X/UTaX_*C*_*/2022/*/*/Chip*/*_mcm.png")
#names = glob.glob("/eos/user/m/mtobin/UT/DATA/IP/U/UTaU_1CT_S3/2022/*/*/Chip*/*_mcm.png")
#names = ["data_200V_-10C_20220828T172454.root"] #glob.glob("/eos/user/m/mtobin/UT/DATA/IP/U/UTaU_1CB_M2/2022/8/28/Chip*/data_200V_-10C_*.root" ) #GT
n2 = glob.glob("." ) #GT
fOut = open('IP_X_new.txt','w')



for fname in names:
    #fSummary = os.path.dirname(n2[0]) + "/bad_channels_list.txt"
    fname = fname.replace("_mcm.png", ".root")
    asicId = fname[fname.find("Chip")+4]
    #i1 = fname.find("STAVE5")
    #i2 = i1 + len("STAVE5x_Sx")
    date = fname[fname.find("2022/")+5] + fname[fname.find("2022/")+7] + fname[fname.find("2022/")+8]
    #sector = fname[i1:i2].replace("_", "" )

    ##### CHaNGE UTb or UTa ###########
    layer = fname[fname.find("UTa")+3]
    stave = fname[fname.find("UTa")+5]
    half = fname[fname.find("UTa")+7]
    length = fname[fname.find("UTa")+9]
    number = fname[fname.find("UTa")+10]
    ew = fname[fname.find("UTa")+11]
    ##### CHaNGE UTb or UTa ###########
    position=0
    print(type(stave))
    if stave == '1':
        if half=="T":
            if ( length=="M" and number == "4"): position = 1
            if ( length=="S" and number == "4"): position = 2
            if ( length=="M" and number == "3"): position = 3
            if ( length=="S" and number == "3"): position = 4
            if ( length=="M" and number == "2"): position = 5
            if ( length=="S" and number == "2"): position = 6
            if ( length=="M" and number == "1"): position = 7
            if ( length=="S" and number == "1"): position = 7
        else:
            if ( length=="M" and number == "4"): position = 14
            if ( length=="S" and number == "4"): position = 13
            if ( length=="M" and number == "3"): position = 12
            if ( length=="S" and number == "3"): position = 11
            if ( length=="M" and number == "2"): position = 10
            if ( length=="S" and number == "2"): position = 9
            if ( length=="M" and number == "1"): position = 8
            if ( length=="S" and number == "1"): position = 8

    else:

        if half=="T":
            if ( length=="M" and number == "4"): position = 1
            if ( length=="S" and number == "3"): position = 2
            if ( length=="M" and number == "3"): position = 3
            if ( length=="S" and number == "2"): position = 4
            if ( length=="M" and number == "2"): position = 5
            if ( length=="S" and number == "1"): position = 6
            if ( length=="M" and number == "1"): position = 7
        else:
            if ( length=="M" and number == "4"): position = 14
            if ( length=="S" and number == "3"): position = 13
            if ( length=="M" and number == "3"): position = 12
            if ( length=="S" and number == "2"): position = 11
            if ( length=="M" and number == "2"): position = 10
            if ( length=="S" and number == "1"): position = 9
            if ( length=="M" and number == "1"): position = 8

    fOut.write(str(position))
    fOut.write("\t")

    #sector = fname[fname.find("UTaU_") : fname.find("UTaU_")+len("UTaU_1CB_M1E")]
    #print("ASIC "+str(asicId))
    fOut.write(layer+"\t"+stave+"\t"+half+"\t"+length+"\t"+number+"\t"+ew+"\t")
    fOut.write(date)
    fOut.write("\t")
    fOut.write(asicId)
    fOut.write("\t")
    badChannelAnalysis( fname )
    fOut.write("\n")

fOut.close()
#c.Print("a.ps]")
#names = glob.glob("/home/ut/data/UTSLICE_STAVE5T_S6/2020/2/18/Chip*/data*.root" )
#print(names)
#names = [ "/home/ut/data/UTSLICE_STAVE5B_S3/2020/2/18/Chip1/data_200V_22C_20200218T165509.root" ]

