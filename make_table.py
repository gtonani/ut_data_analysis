import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import ROOT



input1 = 'IP_X_new.txt'
#input1 = 'MAGNET_X.txt'

data = pd.read_csv(input1, sep="\t", header=None)

#data = pd.concat(map(pd.read_csv, [input1, 'IP_U_2.txt']))

data.columns = ["position", "layer", "stave", "half" , "length", "number", "ew", "date", "chip", "rms_tot", "rms_mcs", "bc0", "bc1", "bc2", "bc3", "bc4", "bc5", "bc6", "bc7", "bc8", "tot_bc"]

print(data)


newdata = data.drop_duplicates(['layer','stave', 'half', 'length', 'number', 'ew', 'chip','date', 'position'], keep='last')
newdata = newdata.drop_duplicates(['layer','stave', 'half', 'length', 'number', 'ew', 'chip'], keep='first')
newdata = newdata[ newdata["half"]=='B']

top_bottom = newdata["half"].iloc[0]
layerr = newdata["layer"].iloc[0]
a_b = 0
if (layerr == 'U' and input1.find("IP")==0) or (layerr == 'X' and input1.find("IP")==0):
	a_b = 'a'
else:
	a_b = 'b'
titolo = "UT"+a_b+str(layerr)+" "+top_bottom+""
titolo_save = "UT"+a_b+str(layerr)+"_"+top_bottom+""


fig_bc1, axes_bc1 = plt.subplots()#figsize=(12,12)
fig_bc1.suptitle(str(titolo)+'- Bad channels - Stave 1 - M1 and S1')
axes_bc1.tick_params(left = False, right = False , labelleft = False , labelbottom = False, bottom = False)


if input1.find("IP")==0:
	fig_bc, axes_bc = plt.subplots(nrows=7,ncols=8,sharex='col',sharey='row',dpi=400 )
	fig_bc.suptitle(str(titolo)+'- Bad channels')

	rms_cms = np.zeros((7,8*4))
	rms_cms_1 = np.zeros((3,8))
	rms_cms_2 = np.zeros((2,8))
	a = np.zeros((14,8))
	b = np.zeros((7,8*4))
	b_1 = np.zeros((3,8))
	b_2 = np.zeros((2,8))

else:
	fig_bc, axes_bc = plt.subplots(nrows=7,ncols=9,sharex='col',sharey='row',dpi=400 )
	fig_bc.suptitle(str(titolo)+'- Bad channels')
	rms_cms = np.zeros((7,9*4))
	rms_cms_1 = np.zeros((3,8))
	rms_cms_2 = np.zeros((2,8))
	a = np.zeros((14,9))
	b = np.zeros((7,9*4))
	b_1 = np.zeros((3,8))
	b_2 = np.zeros((2,8))


for i in range(len(newdata)):
	y = newdata['stave'].iloc[i]
	x = 0
	if newdata['half'].iloc[i]=="T":
		x = newdata['position'].iloc[i]
	else:
		x = newdata['position'].iloc[i]-7

	c = newdata['chip'].iloc[i]

	b[x-1][(y-1)*4+c]= newdata['rms_tot'].iloc[i]
	rms_cms[x-1][(y-1)*4+c]= newdata['rms_mcs'].iloc[i]

	'''n_bad_ch = 10-newdata[newdata=='-'].count(axis=1)
	list_bad_ch = []
	if n_bad_ch.iloc[i]>0:
		for k in range(n_bad_ch.iloc[i]):
			list_bad_ch.append(newdata["bc"+str(k)].iloc[i])'''

	n_bad_ch = newdata['tot_bc'].iloc[i]
	list_bad_ch = []
	if int(n_bad_ch) >0 and int(n_bad_ch) <10:
		for k in range(int(n_bad_ch)):
			list_bad_ch.append(newdata["bc"+str(k)].iloc[i])
	if int(n_bad_ch) >=10:
		list_bad_ch.append(9999)
		list_bad_ch.append(n_bad_ch)


	if newdata['length'].iloc[i]=="M" and newdata['stave'].iloc[i]==1 and newdata['number'].iloc[i]==1:
		if newdata['ew'].iloc[i] == 'W':
			b_1[1][c] = newdata['rms_tot'].iloc[i]
			rms_cms_1[1][c] = newdata['rms_mcs'].iloc[i]
		else:
			b_1[1][c+4] = newdata['rms_tot'].iloc[i]
			rms_cms_1[1][c+4] = newdata['rms_mcs'].iloc[i]

	if newdata['length'].iloc[i]=="S" and newdata['stave'].iloc[i]==1 and newdata['number'].iloc[i]==1:
		if newdata['ew'].iloc[i] == 'W':
			if newdata['half'].iloc[i]=="T":
				b_1[2][c] = newdata['rms_tot'].iloc[i]
				rms_cms_1[2][c] = newdata['rms_mcs'].iloc[i]
			else:
				b_1[0][c] = newdata['rms_tot'].iloc[i]
				rms_cms_1[0][c] = newdata['rms_mcs'].iloc[i]
		else:
			if newdata['half'].iloc[i]=="T":
				b_1[2][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_1[2][c+4] = newdata['rms_mcs'].iloc[i]
			else:
				b_1[0][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_1[0][c+4] = newdata['rms_mcs'].iloc[i]

	if newdata['length'].iloc[i]=="S" and newdata['stave'].iloc[i]==1 and newdata['number'].iloc[i]==2:
		if newdata['ew'].iloc[i] == 'W':
			if newdata['half'].iloc[i]=="T":
				b_1[0][c] = newdata['rms_tot'].iloc[i]
				rms_cms_1[0][c] = newdata['rms_mcs'].iloc[i]
			else:
				b_1[2][c] = newdata['rms_tot'].iloc[i]
				rms_cms_1[2][c] = newdata['rms_mcs'].iloc[i]
		else:
			if newdata['half'].iloc[i]=="T":
				b_1[0][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_1[0][c+4] = newdata['rms_mcs'].iloc[i]
			else:
				b_1[2][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_1[2][c+4] = newdata['rms_mcs'].iloc[i]


	#Stave 2, M1 and S1 divided into E and W

	if newdata['length'].iloc[i]=="M" and newdata['stave'].iloc[i]==2 and newdata['number'].iloc[i]==1:
		if newdata['ew'].iloc[i] == 'W':
			if newdata['half'].iloc[i]=="T":
				b_2[1][c] = newdata['rms_tot'].iloc[i]
				rms_cms_2[1][c] = newdata['rms_mcs'].iloc[i]
			else:
				b_2[0][c] = newdata['rms_tot'].iloc[i]
				rms_cms_2[0][c] = newdata['rms_mcs'].iloc[i]
		else:
			if newdata['half'].iloc[i]=="T":
				b_2[1][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_2[1][c+4] = newdata['rms_mcs'].iloc[i]
			else:
				b_2[0][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_2[0][c+4] = newdata['rms_mcs'].iloc[i]

	if newdata['length'].iloc[i]=="S" and newdata['stave'].iloc[i]==2 and newdata['number'].iloc[i]==1:
		if newdata['ew'].iloc[i] == 'W':
			if newdata['half'].iloc[i]=="T":
				b_2[0][c] = newdata['rms_tot'].iloc[i]
				rms_cms_2[0][c] = newdata['rms_mcs'].iloc[i]
			else:
				b_2[1][c] = newdata['rms_tot'].iloc[i]
				rms_cms_2[1][c] = newdata['rms_mcs'].iloc[i]
		else:
			if newdata['half'].iloc[i]=="T":
				b_2[0][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_2[0][c+4] = newdata['rms_mcs'].iloc[i]
			else:
				b_2[1][c+4] = newdata['rms_tot'].iloc[i]
				rms_cms_2[1][c+4] = newdata['rms_mcs'].iloc[i]


	# stave 1, M1/S1, bad channels
        #########################################################################################################################
	if newdata['stave'].iloc[i]==1 and newdata['number'].iloc[i]==1 and len(list_bad_ch)>0:
		if newdata['ew'].iloc[i] == 'W':
			if newdata['length'].iloc[i]=="M":
				if  newdata['chip'].iloc[i] == 0:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.06, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.06, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')

				if  newdata['chip'].iloc[i] == 1:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.12, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.12, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')

				if  newdata['chip'].iloc[i] == 2:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.18, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.18, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
				if  newdata['chip'].iloc[i] == 3:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.24, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.24, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
	
			else:
				if  newdata['chip'].iloc[i] == 0:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.3, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.3, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')

				if  newdata['chip'].iloc[i] == 1:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.36, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.36, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')

				if  newdata['chip'].iloc[i] == 2:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.42, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.42, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')

				if  newdata['chip'].iloc[i] == 3:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.48, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.48, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
		else:
			if newdata['length'].iloc[i]=="M":
				if  newdata['chip'].iloc[i] == 0:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.54, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.54, str(list_bad_ch[l]),fontsize=1.5)
							axes_bc1.set_facecolor('red')

				if  newdata['chip'].iloc[i] == 1:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.6, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.6, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
				if  newdata['chip'].iloc[i] == 2:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.66, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.66, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
				if  newdata['chip'].iloc[i] == 3:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.72, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.72, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
	
			else:
				if  newdata['chip'].iloc[i] == 0:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.78, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.78, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')

				if  newdata['chip'].iloc[i] == 1:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.84, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.84, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
				if  newdata['chip'].iloc[i] == 2:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.9, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.9, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')
				if  newdata['chip'].iloc[i] == 3:
					if len(list_bad_ch)>0:
						axes_bc1.text(0.02, 0.96, str(newdata['length'].iloc[i])+"1 "+str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=15)
						for l in range(len(list_bad_ch)):
							axes_bc1.text(0.12*(l+1), 0.96, str(list_bad_ch[l]),fontsize=15)
							axes_bc1.set_facecolor('red')

        #########################################################################################################################

	ap = newdata['rms_tot'].iloc[i]
	ap = f'{ap:.3}'

	if newdata['ew'].iloc[i] == 'W':
		#axes_bc[x-1, y-1].text(0.1, 0.1, "W",fontsize=2.5)

		if  newdata['chip'].iloc[i] == 0:

			if len(list_bad_ch)>0:

				axes_bc[x-1, y-1].text(0.05, 0.1, str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=1.5)

				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.1, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.1, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')


		if  newdata['chip'].iloc[i] == 1:

			if newdata['stave'].iloc[i]!=1 and newdata['number'].iloc[i]!=1 and len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.3, str(newdata['ew'].iloc[i])+str(1)+": ",fontsize=1.5)
				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.3, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.3, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')

		if  newdata['chip'].iloc[i] == 2:
			'''if newdata['length'].iloc[i]=="M" and newdata['stave'].iloc[i]==1:
				axes[x-1, y-1].text(0.1, 0.6, str(ap),fontsize=1.5.5)
			if newdata['length'].iloc[i]=="S" and newdata['stave'].iloc[i]==1:
				axes[x-1, y-1].text(0.5, 0.6, str(ap),fontsize=1.5.5)
			else:
				axes[x-1, y-1].text(0.2, 0.6, str(ap),fontsize=2.5)  '''
			if newdata['stave'].iloc[i]!=1 and newdata['number'].iloc[i]!=1 and len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.5, str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=1.5)
				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.5, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.5, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')

		if  newdata['chip'].iloc[i] == 3:
			
			if newdata['stave'].iloc[i]!=1 and newdata['number'].iloc[i]!=1 and len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.7, str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=1.5)
				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.7, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.7, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')

		axes_bc[x-1, y-1].tick_params(left = False, right = False , labelleft = False ,
                labelbottom = False, bottom = False)

		


	if newdata['ew'].iloc[i] == 'E':

		if  newdata['chip'].iloc[i] == 0:
		
			if newdata['stave'].iloc[i]!=1 and newdata['number'].iloc[i]!=1 and len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.2, str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=1.5)
				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.2, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.2, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')

		if  newdata['chip'].iloc[i] == 1:

			if newdata['stave'].iloc[i]!=1 and newdata['number'].iloc[i]!=1 and len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.4, str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=1.5)
				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.4, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.4, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')

		if  newdata['chip'].iloc[i] == 2:
			
			if newdata['stave'].iloc[i]!=1 and newdata['number'].iloc[i]!=1 and len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.6, str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=1.5)  
				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.6, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.6, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')
		if  newdata['chip'].iloc[i] == 3:

			if newdata['stave'].iloc[i]!=1 and newdata['number'].iloc[i]!=1 and len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.8, str(newdata['ew'].iloc[i])+str(c)+": ",fontsize=1.5)
				for l in range(len(list_bad_ch)):
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.2*(0+1), 0.8, "totBC=",fontsize=1.5)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.8, str(list_bad_ch[l]),fontsize=1.5)
						axes_bc[x-1, y-1].set_facecolor('red')

		axes_bc[x-1, y-1].tick_params(left = False, right = False , labelleft = False ,
                labelbottom = False, bottom = False)
		
	



	if newdata['ew'].iloc[i] == '/':
		if  newdata['chip'].iloc[i] == 0:
			if len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.2, str(c)+": ",fontsize=2.)
			for l in range(len(list_bad_ch)):

				axes_bc[x-1, y-1].set_facecolor('red')
				if l>2:
					axes_bc[x-1, y-1].text(0.2*(l-2), 0.1, str(list_bad_ch[l]),fontsize=2.)
				else:
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.14*(0+1), 0.2, "totBC=",fontsize=2.)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.2, str(list_bad_ch[l]),fontsize=2.)
				axes_bc[x-1, y-1].set_facecolor('red')
		if  newdata['chip'].iloc[i] == 1:
			if len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.4, str(c)+": ",fontsize=2.)
			for l in range(len(list_bad_ch)):
				if l>2:
					axes_bc[x-1, y-1].text(0.2*(l-2), 0.3, str(list_bad_ch[l]),fontsize=2.)
				else:
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.14*(0+1), 0.4, "totBC=",fontsize=2.)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.4, str(list_bad_ch[l]),fontsize=2.)
				axes_bc[x-1, y-1].set_facecolor('red')
		if  newdata['chip'].iloc[i] == 2:
			if len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.6, str(c)+": ",fontsize=2.)  
			for l in range(len(list_bad_ch)):
				if l>2:
					axes_bc[x-1, y-1].text(0.2*(l-2), 0.5, str(list_bad_ch[l]),fontsize=2.)
				else:
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.14*(0+1), 0.6, "totBC=",fontsize=2.)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.6, str(list_bad_ch[l]),fontsize=2.)
				axes_bc[x-1, y-1].set_facecolor('red')
		if  newdata['chip'].iloc[i] == 3:
			if len(list_bad_ch)>0:
				axes_bc[x-1, y-1].text(0.05, 0.8, str(c)+": ",fontsize=2.)
			for l in range(len(list_bad_ch)):
				if l>2:
					axes_bc[x-1, y-1].text(0.2*(l-2), 0.7, str(list_bad_ch[l]),fontsize=2.)
				else:
					if l==0 and list_bad_ch[0]==9999:
						axes_bc[x-1, y-1].text(0.14*(0+1), 0.8, "totBC=",fontsize=2.)
					else:
						axes_bc[x-1, y-1].text(0.2*(l+1), 0.8, str(list_bad_ch[l]),fontsize=2.)
				axes_bc[x-1, y-1].set_facecolor('red')
		axes_bc[x-1, y-1].tick_params(left = False, right = False , labelleft = False ,
                labelbottom = False, bottom = False)


if newdata['half'].iloc[i]=="T":	
	axes_bc[6, 0].set_facecolor('black')

else:
	axes_bc[0, 0].set_facecolor('black')


#set M1,S1,S2 to zero

if newdata['half'].iloc[i]=="T":
	for i in range(8):
		for j in range(2):
			b[6-j][i] = 0
			rms_cms[6-j][i] = 0

else:
	for i in range(8):
		for j in range(2):
			b[j][i] = 0
			rms_cms[j][i] = 0


import seaborn as sns
fig, ax = plt.subplots(figsize=(18,12))

if input1.find("IP")==0:
	labels = ['','Stave1                            ', 'Stave2                            ', 'Stave3                            ', 'Stave4                            ', 'Stave5                            ', 'Stave6                            ', 'Stave7                            ', 'Stave8                            ']
else:
	labels = ['','Stave1                            ', 'Stave2                            ', 'Stave3                            ', 'Stave4                            ', 'Stave5                            ', 'Stave6                            ', 'Stave7                            ', 'Stave8                            ','Stave9                            ']

if newdata['half'].iloc[i]=="T":
	labely=['M4          ','S3          ','M3          ','S2          ','M2          ','S1          ','M1          ','']
else:
	labely=['M1          ','S1          ','M2          ', 'S2          ', 'M3          ', 'S3          ', 'M4          ','']

ax = sns.heatmap(b, annot=True,vmin=0, vmax=3.5)
if input1.find("IP")==0:
	plt.xticks(np.arange(0, 33, 4.),labels)
else:
	plt.xticks(np.arange(0, 38, 4.),labels)
plt.yticks(np.arange(0, 8, 1.),labely)#, minor=True)
#plt.yticks(np.arange(0, 8, 1.),labely, minor=False)
ax.yaxis.tick_right()
ax.set_title(str(titolo)+"- RMS total")
ax.grid(which='minor', alpha=0.2)
ax.grid(which='major', alpha=0.8)
plt.savefig(str(titolo_save)+"_rms_tot.png")

#### RMS _ cms ###

fig0, ax0 = plt.subplots(figsize=(18,12))


ax0 = sns.heatmap(rms_cms, annot=True,vmin=0, vmax=3.5)
if input1.find("IP")==0:
	plt.xticks(np.arange(0, 33, 4.),labels)
else:
	plt.xticks(np.arange(0, 38, 4.),labels)

plt.yticks(np.arange(0, 8, 1.),labely)#, minor=True)
ax0.yaxis.tick_right()
ax0.set_title(str(titolo)+ "- RMS cms")

ax0.grid(which='minor', alpha=0.2)
ax0.grid(which='major', alpha=0.8)
plt.savefig(str(titolo_save)+"_rms_cms.png")


#zoom on first stave, M1 and S1

fig2, ax2 = plt.subplots()
label2=['','west                                                            ','east                                                            ']
if newdata['half'].iloc[i]=="T":
	label2y=['S2                    ','M1                    ','S1                    ']
else:
	label2y=['S1                    ','M1                    ','S2                    ']
ax2 = sns.heatmap(b_1, annot=True,vmin=0, vmax=3.5)
plt.xticks(np.arange(0, 9, 4.),label2)
plt.yticks(np.arange(0, 3, 1.),label2y)
ax2.grid()
ax2.set_title( str(titolo)+"- Stave 1 - RMS total")
plt.savefig(str(titolo_save)+"_rms_tot_stave1.png")

#zoom on first stave, M1 and S1 - RMS cms

fig20, ax20 = plt.subplots()

ax20 = sns.heatmap(rms_cms_1, annot=True,vmin=0, vmax=3.5)
plt.xticks(np.arange(0, 9, 4.),label2)
plt.yticks(np.arange(0, 3, 1.),label2y)


ax20.grid()
ax20.set_title(str(titolo)+"- Stave 1 - RMS cms")
plt.savefig(str(titolo_save)+"_rms_cms_stave1.png")


#zoom on second stave, M1 and S1

fig3, ax3 = plt.subplots()
label3=['','west                                                            ','east                                                            ']
if newdata['half'].iloc[i]=="T":
	label3y=['S1                    ','M1                    ']
else:
	label3y=['M1                    ','S1                    ']
ax3 = sns.heatmap(b_2, annot=True,vmin=0, vmax=3.5)
plt.xticks(np.arange(0, 9, 4.),label3)
plt.yticks(np.arange(0, 2, 1.),label3y)
ax3.grid()
ax3.set_title(str(titolo)+"- Stave 2 - RMS total")
plt.savefig(str(titolo_save)+"_rms_tot_stave2.png")

#zoom on second stave, M1 and S1 - RMS cms

fig30, ax30 = plt.subplots()
ax30 = sns.heatmap(rms_cms_2, annot=True,vmin=0, vmax=3.5)
plt.xticks(np.arange(0, 9, 4.),label3)
plt.yticks(np.arange(0, 2, 1.),label3y)
ax30.grid()
ax30.set_title(str(titolo)+"- Stave 2 - RMS cms")
plt.savefig(str(titolo_save)+"_rms_cms_stave2.png")


fig_bc1.savefig(str(titolo_save)+"_bad_channels_00.pdf")
fig_bc.savefig(str(titolo_save)+"_bad_channels.pdf")

plt.show()



