#!/bin/bash

#gzip -d /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_*/2022/8/15/Chip*/*.raw.gz


for i in /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_*/2022/*/*/Chip1/*.raw.gz; do
    gzip -d "$i"

    folder=${i::-3}    
    ./Salt.exe -f "$folder" -a 1 -m mcm -p

    gzip -9 "$folder"

done

for i in /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_*/2022/*/*/Chip0/*.raw.gz; do
    gzip -d "$i"

    folder=${i::-3}    
    ./Salt.exe -f "$folder" -a 0 -m mcm -p

    gzip -9 "$folder"

done

for i in /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_*/2022/*/*/Chip2/*.raw.gz; do
    gzip -d "$i"

    folder=${i::-3}    
    ./Salt.exe -f "$folder" -a 2 -m mcm -p

    gzip -9 "$folder"

done

for i in /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_*/2022/*/*/Chip3/*.raw.gz; do
    gzip -d "$i"

    folder=${i::-3}    
    ./Salt.exe -f "$folder" -a 3 -m mcm -p

    gzip -9 "$folder"

done




#path = /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_M3/2022/8/15/Chip1/*.raw.gz

#gzip -d path

#./Salt.exe -f /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_M3/2022/8/15/Chip1/*.raw -a 1 -m mcm -p

#gzip -9 /eos/user/m/mtobin/UT/DATA/MAGNET/V/UTbV_6C*_M3/2022/8/15/Chip1/*.raw
